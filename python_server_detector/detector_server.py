
import time
import BaseHTTPServer
from pprint import pprint
from SocketServer import ThreadingMixIn
import threading
import cgi
import logging

HOST_NAME = 'ec2-54-93-193-110.eu-central-1.compute.amazonaws.com'
PORT_NUMBER = 8080 


class MyHandler(BaseHTTPServer.BaseHTTPRequestHandler):
        
        def do_GET(s):
                """Respond to a GET request."""
                s.send_response(200)
                s.send_header("Content-type", "text/html")
                s.end_headers()
                s.wfile.write("You made a GET request!")
                
                pprint (vars(s))
        def do_POST(self):
	        # Parse the form data posted
	        form = cgi.FieldStorage(
	            fp=self.rfile, 
	            headers=self.headers,
	            environ={'REQUEST_METHOD':'POST',
	                     'CONTENT_TYPE':self.headers['Content-Type'],
	                     })

	        target = self.headers['Target']

	        # Begin the response
	        self.send_response(200)
	        self.end_headers()
	        #self.wfile.write('Client: %s\n' % str(self.client_address))
	        #self.wfile.write('User-agent: %s\n' % str(self.headers['user-agent']))
	        #self.wfile.write('Path: %s\n' % self.path)
	        #self.wfile.write('Content-Type: ' + self.headers['Content-Type'])
	        
	        logging.warning(self.headers)

	        # Echo back information about what was posted in the form
	        for field in form.keys():
	            field_item = form[field]

	            print type(field_item)

	            if field_item.filename:
	                # The field contains an uploaded file
	                file_data = field_item.file.read()
	                file_len = len(file_data)
	                
	                with open(field_item.filename, "wb") as f:
	                	f.write(file_data)
	               	
	                del file_data

	                # self.wfile.write('\tUploaded %s as "%s" (%d bytes)\n' % \
	                #         (field, field_item.filename, file_len))
	                
	                import subprocess

	                command = "python logo_detector.py " + field_item.filename

	                if target == "brand"

	                shell_command = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE) 
	                result = shell_command.stdout.readlines()[-1]
	                self.wfile.write(result)
	                print result
	                shell_command.wait()
	                
	            else:
	                # Regular form value
	                self.wfile.write('\t%s=%s\n' % (field, form[field].value))
	        return
            
class ThreadedHTTPServer(ThreadingMixIn, BaseHTTPServer.HTTPServer):
    """Handle requests in a separate thread."""

if __name__ == '__main__':
    server_class = BaseHTTPServer.HTTPServer
    httpd = server_class((HOST_NAME, PORT_NUMBER), MyHandler)
    print time.asctime(), "Server Starts - %s:%s" % (HOST_NAME, PORT_NUMBER)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    print time.asctime(), "Server Stops - %s:%s" % (HOST_NAME, PORT_NUMBER)