import numpy as np
import cv2

MIN_MATCH_COUNT = 50

def matchMat(img1, img2):
	# Initiate SURF detector
	surf = cv2.SURF(400)

	# find the keypoints and descriptors with surf
	kp1, des1 = surf.detectAndCompute(img1, None)
	kp2, des2 = surf.detectAndCompute(img2, None)

	FLANN_INDEX_KDTREE = 0
	index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
	search_params = dict(checks = 50)

	flann = cv2.FlannBasedMatcher(index_params, search_params)

	matches = flann.knnMatch(des1, des2, k = 2)

	# store all the good matches as per Lowe's ratio test.
	good = []
	for m,n in matches:
	    if m.distance < 0.6*n.distance:
	        good.append(m)

	if len(good) > MIN_MATCH_COUNT:
		return "success"
	else:
	    return "Not enough matches are found - %d/%d" % (len(good), MIN_MATCH_COUNT)

def findMatch(searchDir, queryImageName):
	queryImage = cv2.imread(queryImageName)

	import os

	for dir in next(os.walk(searchDir))[1]:

		for file in os.listdir(searchDir + dir):
			trainImage = cv2.imread(searchDir + dir + "/" + file, 0)
			if matchMat(trainImage, queryImage) == "success":
				return dir

	return "not found"	


def findBrand(queryImageName):	
	return findMatch('./Logos/', queryImageName)

def findModel(queryImageName, brandName):
	return findMatch('./Models/' + brandName + '/', queryImageName)



#img1 = cv2.imread(sys.argv[2], 0) # trainImage
import sys
if sys.argv[2] == "brand":
	print findBrand(sys.argv[1])
elif sys.argv[2] == "model":
	print findModel(sys.argv[1], sys.argv[3])

#print findLogo(img1, img2)