package org.elsys.thesisdiploma.cammect;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.opencv.android.Utils;
import org.opencv.core.Mat;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

public class PostServerDetector extends AsyncTask<Void, Void, String> {
	public static final String DETECTION_TARGET_BRAND = "brand";
	public static final String DETECTION_TARGET_MODEL = "model";
	
	private final String TAG = "PostServerDetector";

	private OnDetectionResult callback;
	
	private Mat frame;
	private String detectionTarget;
	
	public PostServerDetector(String target, Mat frame, OnDetectionResult callback) {
		this.frame = frame;
		this.callback = callback;
		this.detectionTarget = target;
	}
	
	public PostServerDetector(String target, String brand, Mat frame,
			OnDetectionResult callback) {
		PostServerDetector(target, frame, callback);
	}

	@Override
	protected String doInBackground(Void... params) {
		final File tmpBitmap = saveBitmapTmpFile(convertMatToBitmap(frame));
		String response = null;
		
		try {
			response = uploadBitmap(tmpBitmap);			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return response;
	}

	private String uploadBitmap(File tmpBitmap) throws IOException {
		HttpURLConnection httpURLConnection = null;
		DataOutputStream dataOutputStream = null;
		
		final String urlString = "http://54.93.193.110:8080";
		final String lineEnd = "\r\n";
		final String twoHyphens = "--";
		final String boundary =  "*****";
		
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		final int maxBufferSize = 1*1024*1024;
		
		final FileInputStream fileInputStream = new FileInputStream(tmpBitmap);
		
		final URL url = new URL(urlString);
		Log.i(TAG, "Open Connection");
		httpURLConnection = (HttpURLConnection) url.openConnection();
		
		httpURLConnection.setDoInput(true);
		httpURLConnection.setDoOutput(true);
		httpURLConnection.setUseCaches(false);
		
		httpURLConnection.setRequestMethod("POST");
		
		httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
		httpURLConnection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
		httpURLConnection.setRequestProperty("Target", this.detectionTarget);
		
		Log.i(TAG, "Get OutputStream");
		dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
		dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);;
		dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"uploadedfile\";filename=\"" + tmpBitmap.getName() + "\"" + lineEnd);
		dataOutputStream.writeBytes(lineEnd);
		
		Log.i(TAG, "Write bytes to output");
		bytesAvailable = fileInputStream.available();
		bufferSize = Math.min(bytesAvailable, maxBufferSize);
		buffer = new byte[bufferSize];
		
		bytesRead = fileInputStream.read(buffer, 0, bufferSize);
		
		while (bytesRead > 0) {
			dataOutputStream.write(buffer, 0, bufferSize);
			bytesAvailable = fileInputStream.available();
			bufferSize = Math.min(bytesAvailable, maxBufferSize);
			bytesRead = fileInputStream.read(buffer, 0, bufferSize);
		}
		
		dataOutputStream.writeBytes(lineEnd);
		dataOutputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
		
		final int serverResponseCode = httpURLConnection.getResponseCode();
	    final String serverResponseMessage = httpURLConnection.getResponseMessage();
	    	    
	    Log.i(TAG, serverResponseCode + " " + serverResponseMessage);
	    final String response = readResponse(httpURLConnection);
	    
	    fileInputStream.close();
	    dataOutputStream.flush();
	    dataOutputStream.close();
	    
	    return response;
	}

	private String readResponse(HttpURLConnection httpURLConnection) {
		InputStream inputStream = null;
		
		try {
			inputStream = httpURLConnection.getInputStream();
			
			int ch;
			StringBuffer stringBuffer = new StringBuffer();	
		
			while ((ch = inputStream.read()) != -1) {
				stringBuffer.append((char) ch);
			}
			
			return stringBuffer.toString();
		} catch (IOException e) {
			e.printStackTrace();
			return "error";
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
					return "error";
				}
			}
		}
	}

	private File saveBitmapTmpFile(Bitmap bitmap) {
		FileOutputStream fileOutputStream = null;
		File tmpBitmap = null;
		
		try {
			tmpBitmap = new File(Environment.getExternalStorageDirectory(), "cammect_tmp.png");
			fileOutputStream = new FileOutputStream(tmpBitmap);
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
		} catch (FileNotFoundException e) {
			tmpBitmap = null;
			e.printStackTrace();
		} finally {
			if (fileOutputStream != null) {
				try {
					fileOutputStream.close();
				} catch (IOException e) {
					tmpBitmap = null;
					e.printStackTrace();
				}
			}
		}
		
		return tmpBitmap;
	}

	private Bitmap convertMatToBitmap(Mat input) {
		Bitmap output = Bitmap.createBitmap(input.cols(), input.rows(), Bitmap.Config.ARGB_8888);
		Utils.matToBitmap(input, output);
		
		return output;
	}
	
	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		
		if (result != null) {
			this.callback.onDetectionReult(result);
		}
	}
}
