package org.elsys.thesisdiploma.cammect;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.OpenCVLoader;
import org.elsys.thesisdiploma.cammect.R;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends FragmentActivity implements OnClickListener {
	private final String TAG = "MainActivity";
	private Frame frame;
	private FrameProcess frameProcess;
	private CameraManager cameraManager;

	private CameraBridgeViewBase openCvCameraView;
	private TextView detectionDescriptionTextView;
	private Button detectButton;
	private Button clearButton;
	private ProgressBar progressBar;

	private CarDecetionResultViewerFragment carDecetionResultViewerFragment;
	private Car car;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		super.getWindow().addFlags(
				WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		this.init();
		this.setViewSettings();
		this.addInformationFragment();

	}

	private void addInformationFragment() {
		final FragmentManager fragmentManager = getSupportFragmentManager();
		final FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();

		fragmentTransaction.add(R.id.fragmentPlaceHolder,
				this.carDecetionResultViewerFragment,
				CarDecetionResultViewerFragment.TAG);
		fragmentTransaction.commit();
	}

	private void setViewSettings() {
		this.openCvCameraView.setVisibility(SurfaceView.VISIBLE);
		this.openCvCameraView.setCvCameraViewListener(frame);
		this.detectButton.setOnClickListener(this);
		this.clearButton.setOnClickListener(this);
		this.detectionDescriptionTextView.setOnClickListener(this);
	}

	private void init() {
		this.car = new Car();
		this.carDecetionResultViewerFragment = new CarDecetionResultViewerFragment();
		this.frame = new Frame();
		this.frameProcess = new FrameProcess();
		this.openCvCameraView = (CameraBridgeViewBase) super
				.findViewById(R.id.openCvCameraView);

		this.cameraManager = new CameraManager(this, this.openCvCameraView);

		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_9, this,
				this.cameraManager);

		this.detectButton = (Button) findViewById(R.id.buttonDetect);
		this.clearButton = (Button) findViewById(R.id.buttonClear);
		this.progressBar = (ProgressBar) findViewById(R.id.progressBar);
		this.detectionDescriptionTextView = (TextView) findViewById(R.id.textViewShortItemDescription);
	}

	@Override
	protected void onResume() {
		this.cameraManager.enableView();
		super.onResume();
	}

	@Override
	protected void onPause() {
		this.cameraManager.disableView();
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		this.cameraManager.disableView();
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		MyAlertDialog.showAlertDialogMessage(this,
				"Are you sure you want to exit?", null,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				}, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.buttonDetect:
			if (!this.frameProcess.isDetecting()) {
				this.detect();
			}
			break;

		case R.id.buttonClear:
			this.clearDetection();
			break;
		case R.id.textViewShortItemDescription:
			String urlString = this.car.getFullDescriptionUrl();
			
			if (urlString != null) {
				try {
					URL url = new URL(urlString);
					
				} catch (MalformedURLException e) {
					showToast("There is no more information available.", Toast.LENGTH_SHORT);
					e.printStackTrace();
				}
			}
			break;
		}
	}

	@SuppressWarnings("unused")
	private void openWebViewActivity(String url) {
		
	}
	
	private String getCarInfoFromServer() {
		final String url = getResources().getString(R.string.url_get_car_info)
				.replace(Car.BRAND, car.getBrand())
				.replace(Car.MODEL, car.getModel())
				.replace(Car.DTECTED_OBJECT, car.getDetectedObject());

		new HTTPRequester(url, new HTTPRequesterCallback() {

			@Override
			public void onResponse(String responseBody) {
				detectionDescriptionTextView.setVisibility(TextView.VISIBLE);

				final JSONParser jsonParser = new JSONParser(responseBody);

				if (jsonParser.getValue("Available").equals("yes")) {
					detectionDescriptionTextView.setText(jsonParser
							.getValue("Title")
							+ "\n\n Tap on the text to see more.");
					
					car.setFullDescriptionUrl(jsonParser.getValue("Url"));
				} else {
					detectionDescriptionTextView
							.setText("Information not available.");
				}
			}

			@Override
			public void onError() {
				// TODO Auto-generated method stub

			}
		}).execute();
		return null;
	}

	private void detect() {
		switch (this.frameProcess.getCurrentDetection()) {
		case FrameProcess.DETECT_BRAND:
			this.frameProcess.brandDetect(
					PostServerDetector.DETECTION_TARGET_BRAND,
					this.frame.getCurrentRGBAFrame(), new OnDetectionResult() {

						@Override
						public void onDetectionReult(String itemName) {
							Log.i(TAG, "Brand Detection: " + itemName);

							progressBar.setVisibility(ProgressBar.GONE);

							if (!itemName
									.contains(FrameProcess.DETECT_NOT_FOUND)) {
								Log.i(TAG, "brand found");
								frameProcess
										.setCurrentDetection(FrameProcess.DETECT_MODEL);
								PostServerDetector.CURRENT_BRAND = itemName;

								carDecetionResultViewerFragment
										.setBrandImage(itemName);
								car.setBrand(itemName
										.toLowerCase(Locale.ENGLISH));
								showToast(itemName + " detected.",
										Toast.LENGTH_SHORT);

							} else {
								showToast("Nothing found.", Toast.LENGTH_SHORT);
							}
						}

						@Override
						public void onDetectionResult(float SVMResponse) {
							// DO NOTHING
						}
					});
			break;
		case FrameProcess.DETECT_MODEL:
			this.frameProcess.brandDetect(
					PostServerDetector.DETECTION_TARGET_MODEL,
					this.frame.getCurrentRGBAFrame(), new OnDetectionResult() {

						@Override
						public void onDetectionReult(String itemName) {
							Log.i(TAG, "Model Detection:" + itemName);

							progressBar.setVisibility(ProgressBar.GONE);

							if (!itemName
									.contains(FrameProcess.DETECT_NOT_FOUND)) {
								frameProcess
										.setCurrentDetection(FrameProcess.DETECT_OBJECT);
								carDecetionResultViewerFragment
										.setModelImage(itemName);
								car.setModel(itemName
										.toLowerCase(Locale.ENGLISH));
								showToast(itemName + " found.",
										Toast.LENGTH_SHORT);

							} else {
								showToast("Nothing found.", Toast.LENGTH_SHORT);
							}
						}

						@Override
						public void onDetectionResult(float SVMResponse) {
							// DO NOTHING
						}
					});

			break;
		case FrameProcess.DETECT_OBJECT:
			this.frameProcess.svmDetect(this.frame.getCurrentRGBAFrame(),
					new OnDetectionResult() {

						@Override
						public void onDetectionResult(float svmResponse) {
							Log.i(TAG, "svmResponse: " + svmResponse);
							progressBar.setVisibility(ProgressBar.GONE);

							final String foundObject = SVMResponses
									.getSVMResponseName(svmResponse);

							if (foundObject != null) {
								carDecetionResultViewerFragment
										.setDetectedObjectImage(foundObject);
								car.setDetectedObject(foundObject
										.toLowerCase(Locale.ENGLISH));
								getCarInfoFromServer();
							} else {
								showToast(
										"Nothing detected. You can try again.",
										Toast.LENGTH_SHORT);
							}

						}

						@Override
						public void onDetectionReult(String itemCode) {
							// DO NOTHING
						}
					});
			break;
		}

		this.progressBar.setVisibility(ProgressBar.VISIBLE);
	}
	
	private void showToast(String message, int lenght) {
		Toast.makeText(this, message, lenght).show();
	}

	private void clearDetection() {
		this.car.clear();
		this.frameProcess.clearDetection();
		this.carDecetionResultViewerFragment.clearImageViews();
		this.detectionDescriptionTextView.setText("");
		this.detectionDescriptionTextView.setVisibility(TextView.GONE);
	}

	static {
		System.loadLibrary("opencv_java");
	}
}
