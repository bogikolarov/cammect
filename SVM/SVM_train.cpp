#include <stdio.h>
#include <stdlib.h>

#include <opencv2/opencv.hpp>
#include <opencv2/legacy/legacy.hpp>
#include <opencv2/nonfree/nonfree.hpp>
#include <fstream>
#include <iostream>
#include <string>

#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

using namespace cv;
using namespace std;

Mat readMat(string filepath, string name) {
	FileStorage fileStorage(filepath, FileStorage::READ);

	Mat mat;
	fileStorage[name] >> mat;
	fileStorage.release();

	return mat;
}

int main() {
	initModule_nonfree();

	Mat trainData, labels;

	cout << "Reading Mat objects from file.." << endl;
	trainData = readMat("./BOW/steering_wheel_training_data.xml", "steering_wheel_training_data");
	labels = readMat("./BOW/steering_wheel_labels.xml", "steering_wheel_labels");
	
	cout << "Done..." << endl << "Begin training" << endl;

	CvSVM svm;
	bool res = svm.train_auto(trainData, labels, cv::Mat(), cv::Mat(), svm.get_params());

	cout << "Training done.." << endl << "Saving to classifier.xml" << endl;
	svm.save("steering_wheel_classifier.xml");

	return 0;
}