#include <stdio.h>
#include <stdlib.h>
#include <string>

#include <omp.h>

#include <opencv2/opencv.hpp>
#include <opencv2/legacy/legacy.hpp>
#include <opencv2/nonfree/nonfree.hpp>
#include <fstream>
#include <iostream>
#include <string>

#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

using namespace cv;
using namespace std;

Ptr<FeatureDetector> detector = FeatureDetector::create("SURF");
Ptr<DescriptorExtractor> descriptors = DescriptorExtractor::create("SURF");

string to_string(const int val) {
	int i = val;
	std::string s;
	std::stringstream out;
	out << i;
	s = out.str();
	return s;
}

Mat compute_features(Mat image) {
    vector<KeyPoint> keypoints;
    Mat features;

	detector->detect(image, keypoints);
	KeyPointsFilter::retainBest(keypoints, 1500);
	descriptors->compute(image, keypoints, &features);

	return features;
}

BOWKMeansTrainer addFeaturesToBOWKMeansTrainer(String dir, BOWKMeansTrainer& bowTrainer) {
	DIR *dp;
	struct dirent *dirp;
	struct stat filestat;

	dp = opendir(dir.c_str());

    Mat features;
    Mat img;
 	
    string filepath;
    #pragma loop(hint_parallel(4))
	for (; (dirp = readdir(dp));) {
		filepath = dir + dirp->d_name;

		if (stat( filepath.c_str(), &filestat )) continue;
		if (S_ISDIR( filestat.st_mode ))         continue;

		cout << "Reading... " << filepath << endl;

		img = imread(filepath, 0);

		features = compute_features(img);
		bowTrainer.add(features);
	}
	bowTrainer.cluster();

	return bowTrainer;
}

void computeFeaturesWithBow(string dir, Mat& trainingData, Mat& labels, BOWImgDescriptorExtractor& bowDE, int label) {
	DIR *dp;
	struct dirent *dirp;
	struct stat filestat;

	dp = opendir(dir.c_str());

	vector<KeyPoint> keypoints;
	Mat features;
	Mat img;

	string filepath;

	#pragma loop(hint_parallel(4))
	for (;(dirp = readdir(dp));) {
		filepath = dir + dirp->d_name;

		cout << "Reading: " << filepath << endl;

		if (stat( filepath.c_str(), &filestat )) continue;
		if (S_ISDIR( filestat.st_mode ))         continue;

		img = imread(filepath, 0);

		detector->detect(img, keypoints);
		bowDE.compute(img, keypoints, features);
		KeyPointsFilter::retainBest(keypoints, 1500);

		trainingData.push_back(features);
		labels.push_back((float) label);
	}
	cout << string( 100, '\n' );
}

void predict(string dir, const CvSVM& svm, BOWImgDescriptorExtractor& bowDE) {
	DIR *dp;
	struct dirent *dirp;
	struct stat filestat;

	dp = opendir(dir.c_str());

	string filepath;
	vector<KeyPoint> keyPoints;
	Mat features, img;

	#pragma loop(hint_parallel(4))
	for (;(dirp = readdir(dp));) {
		filepath = dir + dirp->d_name;

		cout << "Reading: " << filepath << "prediction: ";

		if (stat( filepath.c_str(), &filestat )) continue;
		if (S_ISDIR( filestat.st_mode ))         continue;

		img = imread(filepath, 0);
		detector->detect(img, keyPoints);
        bowDE.compute(img, keyPoints, features);

        KeyPointsFilter::retainBest(keyPoints, 1500);
        int i = svm.predict(features);

        cout << i << endl;

	}
}

int main() {
	initModule_nonfree();

    //Ptr<DescriptorMatcher> matcher = DescriptorMatcher::create("FlannBased");
    Ptr<DescriptorMatcher > matcher(new BruteForceMatcher<L2<float> >());

    TermCriteria tc(CV_TERMCRIT_ITER + CV_TERMCRIT_EPS, 10, 0.001);
    int dictionarySize = 1000;
    int retries = 1;
    int flags = KMEANS_PP_CENTERS;
    BOWKMeansTrainer bowTrainer(dictionarySize, tc, retries, flags);
    BOWImgDescriptorExtractor bowDE(descriptors, matcher);

	cout << "Add Features to KMeans" << endl;
	bowTrainer = addFeaturesToBOWKMeansTrainer("./positive_large/", bowTrainer);
	return 0;
	bowTrainer = addFeaturesToBOWKMeansTrainer("./negative_large/", bowTrainer);

	cout << endl << "Clustering..." << endl;

	Mat dictionary = bowTrainer.cluster();
	bowDE.setVocabulary(dictionary);

	Mat labels(0, 1, CV_32FC1);
	Mat trainingData(0, dictionarySize, CV_32FC1);
	

	cout << endl << "Extract bow features" << endl;

	computeFeaturesWithBow("./positive_large/", trainingData, labels, bowDE, 1);
	computeFeaturesWithBow("./negative_large/", trainingData, labels, bowDE, 0);

	CvSVMParams params;
	params.kernel_type=CvSVM::RBF;
	params.svm_type=CvSVM::C_SVC;
	params.gamma=0.50625000000000009;
	params.C=312.50000000000000;
	params.term_crit=cvTermCriteria(TermCriteria::MAX_ITER+TermCriteria::EPS,100,0.000001);
	CvSVM svm;

	cout << endl << "Begin training" << endl;

	bool res=svm.train_auto(trainingData,labels,cv::Mat(),cv::Mat(),svm.get_params());

	cout << endl << "SVM trained" << endl;

	svm.save("classifier.xml");
	
	//CvSVM svm;
	svm.load("classifier.xml");

	cout << "===================== Predicting =====================" << endl;

	predict("./dataset/dev/positive_images", svm, bowDE);
	predict("./dataset/dev/negative_images", svm,bowDE);

/*
	VideoCapture cap(0); // open the default camera

	if(!cap.isOpened())  // check if we succeeded
	    return -1;

    Mat featuresFromCam, grey;
    vector<KeyPoint> cameraKeyPoints;
    namedWindow("edges",1);
    for(int num = 0;;num++)
    {
        Mat frame;
        cap >> frame; // get a new frame from camera
        cvtColor(frame, grey, CV_BGR2GRAY);
        detector->detect(grey, cameraKeyPoints);
        bowDE.compute(grey, cameraKeyPoints, featuresFromCam);

        KeyPointsFilter::retainBest(cameraKeyPoints, 1500);
        int i = svm.predict(featuresFromCam);

        cout << i << endl;

        if (i > 0) {
        	Mat result;
        	drawKeypoints(frame, cameraKeyPoints, result);
        	imwrite("./" + to_string(num) + ".png", result);
        }

        imshow("", frame);
        if(waitKey(30) >= 0) break;
    }	
*/
		return 0;
}