#include <stdio.h>
#include <stdlib.h>
#include <string>

#include <omp.h>

#include <opencv2/opencv.hpp>
#include <opencv2/legacy/legacy.hpp>
#include <opencv2/nonfree/nonfree.hpp>
#include <fstream>
#include <iostream>
#include <string>

#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

using namespace cv;
using namespace std;

Mat compute_features(Mat image, Ptr<FeatureDetector>& detector, Ptr<DescriptorExtractor>& descriptors) {
    vector<KeyPoint> keypoints;
    Mat features;

	detector->detect(image, keypoints);
	KeyPointsFilter::retainBest(keypoints, 1500);
	descriptors->compute(image, keypoints, features);

	return features;
}

BOWKMeansTrainer addFeaturesToBOWKMeansTrainer(String dir, BOWKMeansTrainer& bowTrainer, Ptr<FeatureDetector>& detector, Ptr<DescriptorExtractor>& descriptors) {
	DIR *dp;
	struct dirent *dirp;
	struct stat filestat;

	dp = opendir(dir.c_str());

    Mat features;
    Mat img;
 	
    string filepath;
    #pragma loop(hint_parallel(4))
	for (; (dirp = readdir(dp));) {
		filepath = dir + dirp->d_name;

		if (stat( filepath.c_str(), &filestat )) continue;
		if (S_ISDIR( filestat.st_mode ))         continue;

		cout << "Reading... " << filepath << endl;

		img = imread(filepath);

		cvtColor(img, img, CV_RGBA2BGR);

		features = compute_features(img, detector, descriptors);
		bowTrainer.add(features);
	}

	return bowTrainer;
}

void computeFeaturesWithBow(string dir, Ptr<FeatureDetector>& detector, Mat& trainingData, Mat& labels, BOWImgDescriptorExtractor& bowDE, int label) {
	DIR *dp;
	struct dirent *dirp;
	struct stat filestat;

	dp = opendir(dir.c_str());

	vector<KeyPoint> keypoints;
	Mat features;
	Mat img;

	string filepath;

	#pragma loop(hint_parallel(4))
	for (;(dirp = readdir(dp));) {
		filepath = dir + dirp->d_name;

		cout << "Reading: " << filepath << endl;

		if (stat( filepath.c_str(), &filestat )) continue;
		if (S_ISDIR( filestat.st_mode ))         continue;

		img = imread(filepath);
		cvtColor(img, img, CV_RGBA2BGR);

		detector->detect(img, keypoints);
		bowDE.compute(img, keypoints, features);
		KeyPointsFilter::retainBest(keypoints, 1500);

		trainingData.push_back(features);
		labels.push_back((float) label);
	}
}

void saveMat(string dir, string name, string extension, Mat& dictionary) {
	FileStorage fileStorage(dir + name + extension, FileStorage::WRITE);

	fileStorage << name << dictionary;
	fileStorage.release();
}

																																																																																																																																																																																																																																																																																																																																
int main()
{
	initModule_nonfree();

	cout << "========== BOW Creator ==========" << endl;

	Ptr<FeatureDetector> detector = FeatureDetector::create("DynamicSURF");
	Ptr<DescriptorExtractor> descriptors = DescriptorExtractor::create("OpponentSURF");
	Ptr<DescriptorMatcher> matcher = DescriptorMatcher::create("FlannBased");

	cout << "initialized detector, descriptors and matcher..." << endl;

	TermCriteria tc(CV_TERMCRIT_ITER + CV_TERMCRIT_EPS, 10, 0.001);
    int dictionarySize = 1000;
    int retries = 1;
    int flags = KMEANS_PP_CENTERS;
    BOWKMeansTrainer bowTrainer(dictionarySize, tc, retries, flags);
    BOWImgDescriptorExtractor bowDE(descriptors, matcher);

    cout << "Adding Features to the bag of words..." << endl;
	addFeaturesToBOWKMeansTrainer("./dataset/training/positive_images/", bowTrainer, detector, descriptors); //"./dataset/training/positive_images/"
	addFeaturesToBOWKMeansTrainer("./dataset/training/negative_images/", bowTrainer, detector, descriptors);
	cout << "Done.." << endl;

	cout << endl << "Clustering..." << endl;

	Mat dictionary = bowTrainer.cluster();
	saveMat("./BOW/", "steering_wheel_dictionary", ".xml", dictionary);
	bowDE.setVocabulary(dictionary);

	Mat labels(0, 1, CV_32FC1);
	Mat trainingData(0, dictionarySize, CV_32FC1);	

	cout << endl << "Extract bow features" << endl;

	computeFeaturesWithBow("./dataset/training/positive_images/", detector, trainingData, labels, bowDE, 1);
	computeFeaturesWithBow("./dataset/training/negative_images/", detector, trainingData, labels, bowDE, 0);

	saveMat("./BOW/", "steering_wheel_training_data", ".xml", trainingData);
	saveMat("./BOW/", "steering_wheel_labels", ".xml", labels);

	return 0;
}