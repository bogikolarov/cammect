#include <stdio.h>
#include <stdlib.h>

#include <opencv2/opencv.hpp>
#include <opencv2/legacy/legacy.hpp>
#include <opencv2/nonfree/nonfree.hpp>
#include <fstream>
#include <iostream>
#include <string>

#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

using namespace cv;
using namespace std;

string to_string(const int val) {
	int i = val;
	std::string s;
	std::stringstream out;
	out << i;
	s = out.str();
	return s;
}

Mat readMat(string filepath, string name) {
	FileStorage fileStorage(filepath, FileStorage::READ);

	Mat mat;
	fileStorage[name] >> mat;
	fileStorage.release();

	return mat;
}

Mat computeFeatures(Mat& img) {
	Ptr<FeatureDetector> detector = FeatureDetector::create("DynamicSURF");
	Ptr<DescriptorExtractor> descriptors = DescriptorExtractor::create("OpponentSURF");
	Ptr<DescriptorMatcher> matcher = DescriptorMatcher::create("FlannBased");
	BOWImgDescriptorExtractor bowDE(descriptors, matcher);

	bowDE.setVocabulary(readMat("./BOW/steering_wheel_dictionary.xml", "steering_wheel_dictionary"));

	Mat features;
	vector<KeyPoint> keyPoints;

	detector->detect(img, keyPoints);
	KeyPointsFilter::retainBest(keyPoints, 1500);
    bowDE.compute(img, keyPoints, features);

    return features;
}

void predict(string dir, CvSVM& svm, string class_1_dir, string class_neg1_dir) {
	Ptr<FeatureDetector> detector = FeatureDetector::create("DynamicSURF");
	Ptr<DescriptorExtractor> descriptors = DescriptorExtractor::create("OpponentSURF");
	Ptr<DescriptorMatcher> matcher = DescriptorMatcher::create("FlannBased");
	BOWImgDescriptorExtractor bowDE(descriptors, matcher);

	bowDE.setVocabulary(readMat("./BOW/steering_wheel_dictionary.xml", "steering_wheel_dictionary"));

	DIR *dp;
	struct dirent *dirp;
	struct stat filestat;

	dp = opendir(dir.c_str());

	string filepath;
	vector<KeyPoint> keyPoints;
	Mat features, img;

	int posNum = 0, negNum = 0;

	#pragma loop(hint_parallel(4))
	for (int count = 0;(dirp = readdir(dp));) {
		filepath = dir + dirp->d_name;
		
		if (stat( filepath.c_str(), &filestat )) continue;
		if (S_ISDIR( filestat.st_mode ))         continue;

		cout << count++ << " iteration" << endl;

		img = imread(filepath);
		cvtColor(img, img, CV_RGBA2BGR);
		
		if (img.empty()) {
			continue;
		}

		cout << "image read " << filepath << endl;
		detector->detect(img, keyPoints);
		cout << "feature detect" << endl;
		KeyPointsFilter::retainBest(keyPoints, 1500);
		cout << "retained best features" << endl;
        bowDE.compute(img, keyPoints, features);
        cout << "feature compute" << endl;

        drawKeypoints(img, keyPoints, img);

        int i = svm.predict(features);
        if (i == 0) {
        	imwrite(class_neg1_dir + dirp->d_name, img);
        	negNum++;
        } else {
        	imwrite(class_1_dir + dirp->d_name, img);
        	posNum++;
        }

        cout << "----------" << endl;
        cout << filepath + " prediction: " << to_string(i) << endl;

	}
}



int main() {
	initModule_nonfree();

	CvSVM svm;
	svm.load("steering_wheel_classifier.xml");

	VideoCapture cap(0); // open the default camera

	if(!cap.isOpened())  // check if we succeeded
	    return -1;

    Mat featuresFromCam, grey;
    vector<KeyPoint> cameraKeyPoints;
    namedWindow("edges",1);
    for(int num = 0;;num++)
    {
        Mat frame;
        cap >> frame; // get a new frame from camera
        cvtColor(frame, grey, CV_BGR2GRAY);
        
        int response = svm.predict(computeFeatures(frame));
        cout << response << endl;

        imshow("", frame);
        if(waitKey(30) >= 0) break;
    }	

//	predict("./dataset/training/positive_images/", svm, "./class_1/", "./class_-1/");
//	predict("./dataset/test/positive_images/", svm, "./class_1/", "./class_-1/");


	return 0;
}