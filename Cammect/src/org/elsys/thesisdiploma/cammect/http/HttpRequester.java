package org.elsys.thesisdiploma.cammect.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;
import android.util.Log;

public class HttpRequester extends HttpEntityEnclosingRequestBase {
	public static final int STATUS_OK = 200;
	
	public static final String POST = "POST";
	
	public static final String HEADER_USER_AGENT = "User-Agent";
	public static final String HEADER_CONTENT_TYPE = "Content-Type";
	public static final String HEADER_CONTENT_DISPOSITION = "Content-Disposition";
	public static final String HEADER_CONNECTION = "Connection";
	public static final String HEADER_TARGET = "Target";
	public static final String HEADER_BRAND = "Brand";
	
	public static final String LINE_END = "\r\n";
	public static final String BOUNDARY = "*****";
	public static final String TWO_HYPENS = "--";
	
	public static final String KEEP_ALIVE = "Keep-Alive";
	public static final String USER_AGENT_TEST = "Test";
	
	private final String TAG = getClass().getSimpleName();
	
	private HttpRequesterCallback httpRequesterCallback;
	private HttpClient httpClient;
	private HttpRequestBase httpRequestBase;

	private HttpResponse httpResponse;

	private HttpRequester(HttpRequesterCallback httpRequesterCallback) {
		this.httpClient = new DefaultHttpClient();
		this.httpRequesterCallback = httpRequesterCallback;
	}

	protected HttpRequester(HttpPost httpPost,
			HttpRequesterCallback httpRequesterCallback)
			throws URISyntaxException {
		
		this(httpRequesterCallback);
		this.httpRequestBase = httpPost;
	}

	protected HttpRequester(HttpGet httpGet,
			HttpRequesterCallback httpRequesterCallback)
			throws URISyntaxException {
		
		this(httpRequesterCallback);
		this.httpRequestBase = httpGet;
	}

	protected HttpRequestBase getHttpRequestBase() {
		return this.httpRequestBase;
	}

	@Override
	public String getMethod() {
		if (this.httpRequestBase != null) {
			return this.httpRequestBase.getMethod();
		}
		return null;
	}

	public void addHeader(String name, String value) {
		this.httpRequestBase.addHeader(name, value);
	}

	public void execute() {
		new HttpRequestExecuter().execute();
	}

	private class HttpRequestExecuter extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {
			try {
				httpResponse = httpClient.execute(httpRequestBase);
				return getResponseBody(httpResponse);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return "error";
		}

		@Override
		protected void onPostExecute(String result) {
			if (httpResponse != null) {
				if (httpResponse.getStatusLine().getStatusCode() == HttpRequester.STATUS_OK && httpRequesterCallback != null) {
					httpRequesterCallback.onResponse(result);
				} else {
					Log.i(TAG, result);
				}
			} else {
				if (httpRequesterCallback != null) {
					httpRequesterCallback.onError(result);
				} else {
					Log.i(TAG, result);
				}
			}
		}

		private String getResponseBody(HttpResponse httpResponse)
				throws IOException {
			final BufferedReader bufferedReader = new BufferedReader(
					new InputStreamReader(
							httpResponse.getEntity().getContent(), "UTF-8"));
			final StringBuilder stringBuilder = new StringBuilder();

			String line;

			while ((line = bufferedReader.readLine()) != null) {
				stringBuilder.append(line);
			}

			return stringBuilder.toString();
		}
	}
	
	public static void checkInternetConnection(final InternetCheckCallback callback) {
		new AsyncTask<Void, Void, Integer>() {

			@Override
			protected Integer doInBackground(Void... params) {
				try {
					final HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(URLs.GOOGLE).openConnection();
					httpURLConnection.setRequestProperty(HttpRequester.HEADER_USER_AGENT, HttpRequester.USER_AGENT_TEST);
					httpURLConnection.setConnectTimeout(1500);
					httpURLConnection.connect();
					return httpURLConnection.getResponseCode();
				} catch (IOException e) {
					e.printStackTrace();
				}
				return null;
			}

			@Override
			protected void onPostExecute(Integer result) {
				if (result != null && result == STATUS_OK) {
					callback.hasConnection(true);
				} else {
					callback.hasConnection(false);
				}
			}
			
		}.execute();
	}
}

