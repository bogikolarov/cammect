package org.elsys.thesisdiploma.cammect.http;

public interface HttpRequesterCallback {
	public void onResponse(String responseMessage);
	public void onError(String responseError);
}
