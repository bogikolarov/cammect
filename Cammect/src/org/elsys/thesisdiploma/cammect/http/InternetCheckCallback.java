package org.elsys.thesisdiploma.cammect.http;

public interface InternetCheckCallback {
	public void hasConnection(boolean hasConnection);
}
