package org.elsys.thesisdiploma.cammect.http;

import java.net.URISyntaxException;

import org.apache.http.client.methods.HttpGet;

public class HttpGetRequester extends HttpRequester {

	public HttpGetRequester(HttpGet httpGet,
			HttpRequesterCallback httpRequesterCallback)
			throws URISyntaxException {
		super(httpGet, httpRequesterCallback);
		
	}

}
