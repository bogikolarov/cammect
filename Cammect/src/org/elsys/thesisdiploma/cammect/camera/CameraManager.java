package org.elsys.thesisdiploma.cammect.camera;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;

import android.content.Context;
import android.util.Log;

public class CameraManager extends BaseLoaderCallback {
	private final String TAG = "CameraManager";
	private final CameraBridgeViewBase cameraBridgeViewBase;
	
	public CameraManager(Context appContext, CameraBridgeViewBase cameraBridgeViewBase) {
		super(appContext);
		this.cameraBridgeViewBase = cameraBridgeViewBase;
	}

	@Override
	public void onManagerConnected(int status) {
		switch (status) {
		case LoaderCallbackInterface.SUCCESS: {
			Log.i(TAG, "OpenCV loaded successfully");
			this.enableView();
		}
			break;
		default: {
			super.onManagerConnected(status);
		}
			break;
		}
	}

	public void enableView() {
		if (this.cameraBridgeViewBase != null) {
			this.cameraBridgeViewBase.enableView();
		}
	}
	
	public void disableView() {
		if (this.cameraBridgeViewBase != null) {
			this.cameraBridgeViewBase.disableView();
		}
	}
}
