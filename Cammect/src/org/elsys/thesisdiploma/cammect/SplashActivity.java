package org.elsys.thesisdiploma.cammect;

import java.util.ArrayList;
import java.util.Iterator;
import org.elsys.thesisdiploma.cammect.alertdialog.MyAlertDialog;
import org.elsys.thesisdiploma.cammect.detection.NativeSVMDetector;
import org.elsys.thesisdiploma.cammect.file.FileDownloadCallback;
import org.elsys.thesisdiploma.cammect.file.FileManager;
import org.elsys.thesisdiploma.cammect.http.HttpRequester;
import org.elsys.thesisdiploma.cammect.http.InternetCheckCallback;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;
import android.widget.TextView;

public class SplashActivity extends Activity implements FileDownloadCallback {
	private final String TAG = getClass().getSimpleName();

	private final int RESULT_OK = 0;

	private TextView loadingTextView;
	private CountDownTimer splashScreenTimer;
	
	private int lastDownloaderThread;
	private boolean timerFinished = false;
	private boolean appPrepared = false;
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == RESULT_OK) {
			try {
				Thread.sleep(3500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			this.checkInternetConnection();
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);

		this.init();
		this.startTimer();	
		this.checkInternetConnection();
	}

	private void init() {
		this.loadingTextView = (TextView) super
				.findViewById(R.id.textViewLoading);
		this.splashScreenTimer = new CountDownTimer(2000, 1000) {
			
			@Override
			public void onTick(long millisUntilFinished) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onFinish() {
				timerFinished = true;
				
				if (appPrepared) {
					startMainActivity();
				}
			}
		};
	}
	
	private void startTimer() {
		this.splashScreenTimer.start();
	}

	private void checkInternetConnection() {
		this.loadingTextView.setText("Checking connection...");
		final Context context = this;
		HttpRequester.checkInternetConnection(new InternetCheckCallback() {

			@Override
			public void hasConnection(boolean hasConnection) {
				if (hasConnection) {
					ensureNecessaryFiles();
				} else {

					MyAlertDialog.createAlertDialog(context,
							"No internet access available.",
							"Would you like to open settings?",
							MyAlertDialog.YES,
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									startActivityForResult(new Intent(
											Settings.ACTION_WIFI_SETTINGS),
											RESULT_OK);
								}
							}, 
							MyAlertDialog.NO,
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									finish();
								}
							}).show();
				}
			}
		});

	}

	private void ensureNecessaryFiles() {
		Log.i(TAG, "ensureNecessaryFiles");
		final String filePath = Environment.getExternalStorageDirectory() + "/" + FileManager.APP_SUBDIRECTORY;
		final ArrayList<String> filesToDownload = new ArrayList<String>();
		
		this.loadingTextView.setText("Checking for missing necessary files...");
		
		filesToDownload.add(NativeSVMDetector.FILE_DICTIONARY);
		filesToDownload.add(NativeSVMDetector.FILE_CLASSIFIER);

		for (Iterator<String> iterator = filesToDownload.iterator(); iterator
				.hasNext();) {
			if (FileManager.fileExists(filePath + iterator.next())) {
				iterator.remove();
			}
		}

		if (filesToDownload.size() > 0) {
			Log.i(TAG, "files needs to be downloaded");
			final FileManager fileManager = new FileManager();
			final String baseUrl = 
					super.getResources().getString(R.string.url_get_detection_files);

			for (String file : filesToDownload) {
				Log.i(TAG, "downloading " + file);
				
				this.loadingTextView.setText("Downloading files...");
				this.lastDownloaderThread = fileManager.downloadFile(baseUrl
						+ file, filePath, file, this);
			}
		} else {
			this.appPrepared = true;
			this.startMainActivity();
		}
	}

	@Override
	public void onFileDownloaded(String fileDir, String fileName,
			int threadHashCode) {
		if (this.lastDownloaderThread == threadHashCode) {
			this.appPrepared = true;
			this.startMainActivity();
		}
	}

	@Override
	public void onFileDownloadError() {
		MyAlertDialog.createAlertDialog(this, "Error occured while downloading the files.", null, MyAlertDialog.OK, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog,
					int which) {
				finish();
			}
		}).show();
	}
	
	private void startMainActivity() {
		this.loadingTextView.setText("Loading...");
		if (this.timerFinished) {
			startActivity(new Intent(this, MainActivity.class));
			this.finish();
		}
	}

	@Override
	public void onBackPressed() {
		
	}
	
	@Override
	public void finish() {
		super.finish();
		super.overridePendingTransition(android.R.anim.fade_in, R.anim.animation_drop_out_window);
	}
}
