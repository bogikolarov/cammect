package org.elsys.thesisdiploma.cammect;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebViewActivity extends Activity {
	private WebView webView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_web_view);
		
		this.init();
		this.setWebViewSettings();
		
		if (getIntent() != null) {
			this.webView.loadUrl(super.getIntent().getStringExtra("url"));
		}
	}

	@SuppressLint("SetJavaScriptEnabled")
	private void setWebViewSettings() {
		final WebSettings webSettings = this.webView.getSettings();
		webSettings.setJavaScriptEnabled(true);
		webSettings.setDomStorageEnabled(true);
		
		this.webView.setWebChromeClient(new WebChromeClient());
		this.webView.setWebViewClient(new WebViewClient());
	}

	private void init() {
		this.webView = (WebView) findViewById(R.id.webView);
	}

	@Override
	public void onBackPressed() {
		if (this.webView.canGoBack()) {
			this.webView.goBack();
		} else {
			startMainActivity();
			super.onBackPressed();
		}
	}

	private void startMainActivity() {
		final Intent intent = new Intent(this, MainActivity.class);
		final Bundle bundleAnimation = ActivityOptionsCompat
				.makeCustomAnimation(this, R.anim.animation_enter_from_left, R.anim.animation_exit_from_left).toBundle();

		super.startActivity(intent, bundleAnimation);
	}

	@Override
	public void finish() {
		this.webView.destroy();
		super.finish();
	}
}
