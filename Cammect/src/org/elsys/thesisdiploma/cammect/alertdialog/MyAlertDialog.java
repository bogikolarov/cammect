package org.elsys.thesisdiploma.cammect.alertdialog;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

public class MyAlertDialog {
	public static final String YES = "Yes";
	public static final String NO = "No";
	public static final String OK = "Ok";
	
	public static AlertDialog createAlertDialog(Context context, String title,
			String message, String positiveText,
			final DialogInterface.OnClickListener positiveListener,
			String negativeText,
			final DialogInterface.OnClickListener negativeListener) {
		
		Builder alertDialog = new AlertDialog.Builder(context)
				.setTitle(title)
				.setCancelable(false)
				.setPositiveButton(positiveText, new OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						positiveListener.onClick(dialog, which);
						
					}
				});
				
		if (message != null) {
			alertDialog.setMessage(message);
		}
		
		if (negativeListener != null) {
			alertDialog.setNegativeButton(negativeText, new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					negativeListener.onClick(dialog, which);
					
				}
			});
		}
		
		return alertDialog.create();
	}
	
	public static AlertDialog createAlertDialog(Context context, String title,
			String message, String positiveText,
			final DialogInterface.OnClickListener positiveListener) {
		return createAlertDialog(context, title, message, positiveText, positiveListener, null, null);
	}
}
