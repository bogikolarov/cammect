package org.elsys.thesisdiploma.cammect.detection;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import org.elsys.thesisdiploma.cammect.file.FileManager;
import org.elsys.thesisdiploma.cammect.file.FileUploadCallback;
import org.elsys.thesisdiploma.cammect.http.HttpRequester;
import org.elsys.thesisdiploma.cammect.http.URLs;
import org.opencv.android.Utils;
import org.opencv.core.Mat;

import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;

public class ServerDetector {
	public static final String DETECTION_TARGET_BRAND = "brand";
	public static final String DETECTION_TARGET_MODEL = "model";
	
	private String currentBrand = null;

	private final String TAG = getClass().getSimpleName();

	private FileManager fileManager;

	public ServerDetector() {
		this.fileManager = new FileManager();
	}
	
	public void detect(Mat frame, String target, HashMap<String, String> requestProperties, final OnDetectionResult callback) {
		Log.i(TAG, "ServerDetector detect(): " + target);
		final Bitmap frameBmp = this.convertMatToBitmap(frame);
		final File frameFile = this.saveBitmapTmpFile(frameBmp);
		
		if (requestProperties == null) {
			requestProperties = new HashMap<String, String>();
		}
		
		requestProperties.put(HttpRequester.HEADER_CONNECTION, HttpRequester.KEEP_ALIVE);
		requestProperties.put(HttpRequester.HEADER_CONTENT_TYPE, "multipart/form-data;boundary=" + HttpRequester.BOUNDARY);
		requestProperties.put(HttpRequester.HEADER_TARGET, target);

		this.fileManager.uploadFile(frameFile, URLs.DETECTION_SERVER, requestProperties, new FileUploadCallback() {
			
			@Override
			public void onFileUploaded(String response) {
				Log.i(TAG, "ServerDetector onFileUploaded: " + response);
				callback.onDetectionReult(response);
				frameFile.delete();
			}
			
			@Override
			public void onError() {
				callback.onError("network error");
			}
		});
	}

	private File saveBitmapTmpFile(Bitmap bitmap) {
		final String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());
		final String fileName = "CAMMECT_" + timeStamp + "_TMP.png";
		final String filePath = Environment.getExternalStorageDirectory()
				.getAbsolutePath() + "/";
		
		FileOutputStream fileOutputStream = null;
		File tmpBitmap = null;

		try {
			tmpBitmap = new File(filePath, fileName);
			fileOutputStream = new FileOutputStream(tmpBitmap);
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
		} catch (FileNotFoundException e) {
			tmpBitmap = null;
			e.printStackTrace();
		} finally {
			if (fileOutputStream != null) {
				try {
					fileOutputStream.close();
				} catch (IOException e) {
					tmpBitmap = null;
					e.printStackTrace();
				}
			}
		}

		return tmpBitmap;
	}

	private Bitmap convertMatToBitmap(Mat input) {
		final Bitmap output = Bitmap.createBitmap(input.cols(), input.rows(),
				Bitmap.Config.ARGB_8888);
		Utils.matToBitmap(input, output);

		return output;
	}
	
	public void setCurrentBrand(String brand) {
		this.currentBrand = brand;
	}
	
	public String getCurrentBrand() {
		return this.currentBrand;
	}
}
