package org.elsys.thesisdiploma.cammect.detection;

import java.util.Locale;

import org.elsys.thesisdiploma.cammect.car.Car;
import org.elsys.thesisdiploma.cammect.car.CarDecetionResultViewerFragment;
import org.elsys.thesisdiploma.cammect.frame.FrameProcess;

import android.content.Context;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.Toast;

public class DetectionResultHandler implements OnDetectionResult {
	private final String TAG = getClass().getSimpleName();

	private Context context;
	private ProgressBar progressBar;
	private CarDecetionResultViewerFragment carDecetionResultViewerFragment;
	private Car car;
	private DetectedObjectCallback detectedObjectCallback;
	
	private int currentDetection = FrameProcess.DETECT_BRAND;
	
	public DetectionResultHandler(Context context, ProgressBar progressBar,
			CarDecetionResultViewerFragment carDecetionResultViewerFragment, DetectedObjectCallback detectedObjectCallback) {
		this.context = context;
		this.progressBar = progressBar;
		this.carDecetionResultViewerFragment = carDecetionResultViewerFragment;
		this.car = new Car();
		this.detectedObjectCallback = detectedObjectCallback;
	}

	@Override
	public void onDetectionResult(float svmResponse) {
		this.hideProgressBar();
		final String foundObject = SVMResponses.getSVMResponseName(svmResponse);

		if (foundObject != null) {
			Log.i(TAG, "SVM Detection: " + svmResponse);
			this.carDecetionResultViewerFragment.setDetectedObjectImage(foundObject);
			this.car.setDetectedObject(foundObject.toLowerCase(Locale.ENGLISH));
			this.detectedObjectCallback.onDetectedObjectAvailable(this.car);
		} else {
			showToast("Nothing detected. You can try again.",
					Toast.LENGTH_SHORT);
		}
	}

	@Override
	public void onDetectionReult(String itemName) {
		this.hideProgressBar();

		if (!itemName.contains(FrameProcess.DETECT_NOT_FOUND)) {
			
			Log.i(TAG, "" + this.currentDetection);
			switch (this.currentDetection) {
			case FrameProcess.DETECT_BRAND:
				this.carDecetionResultViewerFragment.setBrandImage(itemName);
				this.car.setBrand(itemName.toLowerCase(Locale.ENGLISH));
				Log.i(TAG, "Handler Current Detection: BRAND " + this.car.getBrand());
				break;
			case FrameProcess.DETECT_MODEL:
				this.carDecetionResultViewerFragment.setModelImage(itemName);
				this.car.setModel(itemName);
				Log.i(TAG, "Handler Current Detection: MODEl " + this.car.getModel());
				break;
			}

			this.showToast(itemName + " found", Toast.LENGTH_SHORT);
		} else {
			this.showToast("not found", Toast.LENGTH_SHORT);
		}
	}

	private void showToast(String message, int length) {
		Toast.makeText(this.context, message, length).show();
	}

	@Override
	public void onError(String message) {
		this.hideProgressBar();
	}

	private void hideProgressBar() {
		this.progressBar.setVisibility(ProgressBar.GONE);
	}
	
	public void setCurrentDetection(int currentDetection) {
		Log.i(TAG, "Handler setCurrentDetection(): " + currentDetection);
		this.currentDetection = currentDetection;
	}
	
	public Car getCar() {
		return this.car;
	}
	
	public void clear() {
		this.car = new Car();
	}
}
