package org.elsys.thesisdiploma.cammect.detection;

import org.elsys.thesisdiploma.cammect.file.FileManager;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;

import android.os.AsyncTask;
import android.os.Environment;

public class NativeSVMDetector extends AsyncTask<Void, Void, Float> {
	public static final String FILE_DICTIONARY = "dictionary.xml";
	public static final String FILE_CLASSIFIER = "steering_wheel_classifier.xml";
	private Mat frame;
	private OnDetectionResult callback;

	private boolean isDetecting;

	public NativeSVMDetector(Mat frame, OnDetectionResult callback) {
		this.frame = frame;
		this.callback = callback;
		this.isDetecting = false;
	}

	public boolean isDetecting() {
		return this.isDetecting;
	}

	@Override
	protected Float doInBackground(Void... params) {
		final Mat mat = frame.clone();

		final float svmResult = SVMDetect(mat.getNativeObjAddr(), 
								Environment.getExternalStorageDirectory().getAbsolutePath() + FileManager.APP_SUBDIRECTORY, 
								NativeSVMDetector.FILE_DICTIONARY, NativeSVMDetector.FILE_CLASSIFIER);

		// Highgui.imwrite(Environment.getExternalStorageDirectory().getAbsolutePath()
		// + "/result.png", frame);
		return svmResult;
	}

	@Override
	protected void onPostExecute(Float svmResponse) {
		this.isDetecting = false;
		this.callback.onDetectionResult(svmResponse.floatValue());
		super.onPostExecute(svmResponse);
	}

	public native float SVMDetect(long nativeFrameAddr, String dictionaryPath,
			String dictionaryName, String classifierName);

	static {
		System.loadLibrary("opencv_java");
		System.loadLibrary("nonfree");
		System.loadLibrary("svm_detector");
	}

}
