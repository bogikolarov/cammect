package org.elsys.thesisdiploma.cammect.detection;

public interface OnDetectionResult {
	public void onDetectionResult(float SVMResponse);
	public void onDetectionReult(String itemName);
	public void onError(String message);
}
