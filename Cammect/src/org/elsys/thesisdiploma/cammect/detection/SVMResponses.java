package org.elsys.thesisdiploma.cammect.detection;

import android.util.Log;

public class SVMResponses {
	private static final int STEERING_WHEEL_CODE = 1;
	
	private static final String TAG = "SVMResponses";
	private static final String  STEERING_WHEEL = "steering_wheel";
	
	public static String getSVMResponseName(float response) {
		switch(Math.round(response)) {
		case SVMResponses.STEERING_WHEEL_CODE:
			Log.i(TAG, "" + Math.round(response));
			return SVMResponses.STEERING_WHEEL;
			
		default:
				return null;
		}
	}	
}
