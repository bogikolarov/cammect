package org.elsys.thesisdiploma.cammect.detection;

import org.elsys.thesisdiploma.cammect.car.Car;

public interface DetectedObjectCallback {
	public void onDetectedObjectAvailable(Car car);
}
