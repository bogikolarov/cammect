package org.elsys.thesisdiploma.cammect;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.OpenCVLoader;
import org.apache.http.client.methods.HttpGet;
import org.elsys.thesisdiploma.cammect.R;
import org.elsys.thesisdiploma.cammect.alertdialog.MyAlertDialog;
import org.elsys.thesisdiploma.cammect.camera.CameraManager;
import org.elsys.thesisdiploma.cammect.car.Car;
import org.elsys.thesisdiploma.cammect.car.CarDecetionResultViewerFragment;
import org.elsys.thesisdiploma.cammect.detection.DetectedObjectCallback;
import org.elsys.thesisdiploma.cammect.detection.DetectionResultHandler;
import org.elsys.thesisdiploma.cammect.detection.ServerDetector;
import org.elsys.thesisdiploma.cammect.frame.Frame;
import org.elsys.thesisdiploma.cammect.frame.FrameProcess;
import org.elsys.thesisdiploma.cammect.http.HttpGetRequester;
import org.elsys.thesisdiploma.cammect.http.HttpRequesterCallback;
import org.elsys.thesisdiploma.cammect.http.URLs;
import org.elsys.thesisdiploma.cammect.json.JSONParser;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends FragmentActivity implements OnClickListener,
		DetectedObjectCallback {
	
	private final String TAG = getClass().getSimpleName();

	private Frame frame;
	private FrameProcess frameProcess;
	private CameraManager cameraManager;
	private CameraBridgeViewBase openCvCameraView;
	
	private Button detectButton;
	private Button clearButton;
	private ProgressBar progressBar;

	private CarDecetionResultViewerFragment carDecetionResultViewerFragment;

	private DetectionResultHandler detectionResultHandler;
	
	private final List<Integer> blockedKeys = new ArrayList<Integer>(Arrays.asList(KeyEvent.KEYCODE_VOLUME_DOWN, KeyEvent.KEYCODE_VOLUME_UP));
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		super.getWindow().addFlags(
				WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	
		this.init();
		this.setViewSettings();
		this.addInformationFragment();
	}

	private void addInformationFragment() {
		final FragmentManager fragmentManager = getSupportFragmentManager();
		final FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();

		fragmentTransaction.add(R.id.fragmentPlaceHolder,
				this.carDecetionResultViewerFragment,
				CarDecetionResultViewerFragment.TAG);
		fragmentTransaction.commit();
	}

	private void setViewSettings() {
		this.openCvCameraView.setVisibility(SurfaceView.VISIBLE);
		this.openCvCameraView.setCvCameraViewListener(frame);
		this.detectButton.setOnClickListener(this);
		this.clearButton.setOnClickListener(this);
	}

	private void init() {
		this.carDecetionResultViewerFragment = new CarDecetionResultViewerFragment();
		this.frame = new Frame();
		this.openCvCameraView = (CameraBridgeViewBase) super
				.findViewById(R.id.openCvCameraView);

		this.cameraManager = new CameraManager(this, this.openCvCameraView);

		this.detectButton = (Button) super.findViewById(R.id.buttonDetect);
		this.clearButton = (Button) super.findViewById(R.id.buttonClear);
		this.progressBar = (ProgressBar) super.findViewById(R.id.progressBar);
		
		this.detectionResultHandler = new DetectionResultHandler(
				super.getApplicationContext(), this.progressBar,
				this.carDecetionResultViewerFragment, this);
		this.frameProcess = new FrameProcess(this.detectionResultHandler);
	}

	@Override
	protected void onResume() {
		if (!OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_9, this,
				this.cameraManager)) {
			this.showToast("Camera error", Toast.LENGTH_LONG);
			super.finish();
		}
		
		super.onResume();
	}

	@Override
	protected void onPause() {
		this.cameraManager.disableView();
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		this.cameraManager.disableView();
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {		
		MyAlertDialog.createAlertDialog(this, 
				"Are you sure you want to exit?", null, 
				"Yes", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				}, 
				"No", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).show();
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.buttonDetect:
			if (!this.frameProcess.isDetecting()) {
				this.detect();
			} else {
				this.showToast("Already scanning.", Toast.LENGTH_SHORT);
			}

			break;
		case R.id.buttonClear:
			this.clearDetection();
			break;
		}
	}

	private void openWebViewActivity(String url) {
		this.progressBar.setVisibility(ProgressBar.VISIBLE);
		final Intent intent = new Intent(this, WebViewActivity.class);
		intent.putExtra("url", url);

		final Bundle bundleAnimation = ActivityOptionsCompat
				.makeCustomAnimation(this, R.anim.animation_enter_from_right, R.anim.animation_exit_from_right)
				.toBundle();

		super.startActivity(intent, bundleAnimation);
		super.finish();
	}

	@Override
	public void onDetectedObjectAvailable(final Car car) {
		Log.i(TAG, "onDetectedObjectAvailable");
		this.getCarInfoFromServer(car);
	}

	private void getCarInfoFromServer(final Car car) {
		final String url = URLs.INFO_URL_TEMPLATE
						.replace(Car.BRAND, car.getBrand())
						.replace(Car.MODEL, car.getModel())
						.replace(Car.DTECTED_OBJECT, car.getDetectedObject());

		try {
			new HttpGetRequester(new HttpGet(url), new HttpRequesterCallback() {

				@Override
				public void onResponse(String responseMessage) {
					final JSONParser jsonParser = new JSONParser(
							responseMessage);

					if (jsonParser.getValue("Available").equals("yes")) {
						car.setDescription(jsonParser.getValue("Title"));
						car.setFullDescriptionUrl(jsonParser.getValue("Url"));
						
						showInformationDialog(car);
					} else {
						Toast.makeText(getApplicationContext(), "No more information available.", Toast.LENGTH_SHORT).show();
					}
				}

				@Override
				public void onError(String responseError) {

				}

			}).execute();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}

	private void showInformationDialog(final Car car) {
		final String brand = car.getBrand();
		final StringBuilder title = new StringBuilder();
		title.append(Character.toUpperCase(brand.toCharArray()[0]));
		title.append(brand.substring(1, brand.length()));
		title.append(" ");
		title.append(car.getModel().toUpperCase(Locale.getDefault()));
		
		final String description = car.getDescription() + "\n\n Would you like to read more about it?";
		
		MyAlertDialog.createAlertDialog(this, title.toString(), description, 
				MyAlertDialog.YES, new AlertDialog.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						final String urlString = car.getFullDescriptionUrl();

						if (urlString != null) {
							try {
								new URL(urlString);
								openWebViewActivity(urlString);
							} catch (MalformedURLException e) {
								showToast(
										"There is no more information available.",
										Toast.LENGTH_SHORT);
								e.printStackTrace();
								dialog.dismiss();
							}
						}
						
					}
				}, MyAlertDialog.NO, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						clearDetection();
						dialog.dismiss();
					}
				}).show();
	}
	
	private void detect() {
		Log.i(TAG, "============ DETECTION ============");
		this.progressBar.setVisibility(ProgressBar.VISIBLE);
		switch (this.frameProcess.getCurrentDetection()) {
		case FrameProcess.DETECT_BRAND:
			this.frameProcess.brandDetect(
					ServerDetector.DETECTION_TARGET_BRAND,
					this.frame.getCurrentRGBAFrame());
			Log.i(TAG, "MainActivity Detect Brand");
			break;
		case FrameProcess.DETECT_MODEL:
			this.frameProcess.modelDetect(
					ServerDetector.DETECTION_TARGET_MODEL,
					this.frame.getCurrentRGBAFrame());
			Log.i(TAG, "MainActivity Detect Model");
			break;
		case FrameProcess.DETECT_OBJECT:
			this.frameProcess.svmDetect(this.frame.getCurrentRGBAFrame());
			Log.i(TAG, "MainActivity Detect Object");
			break;
		}
	}

	private void showToast(String message, int length) {
		Toast.makeText(this, message, length).show();
	}

	private void clearDetection() {
		this.carDecetionResultViewerFragment.clearImageViews();
		this.frameProcess.clearDetection();
		this.detectionResultHandler.clear();
	}
	
	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		if (blockedKeys.contains(event.getKeyCode())) {
			return true;
		} else {
			return super.dispatchKeyEvent(event);
		}
	}

	static {
		System.loadLibrary("opencv_java");
	}
}
