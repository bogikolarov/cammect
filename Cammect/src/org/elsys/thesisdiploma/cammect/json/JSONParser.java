package org.elsys.thesisdiploma.cammect.json;

import org.json.JSONException;
import org.json.JSONObject;

public class JSONParser {
	private JSONObject jsonObject;
	
	public JSONParser(String jsonString) {
		try {
			this.jsonObject = new JSONObject(jsonString);
		} catch (JSONException e) {
			e.printStackTrace();
			this.jsonObject = null;
		}
	}

	public String getValue(String key) {
		if (this.jsonObject != null && this.jsonObject.has(key)) {
			try {
				return this.jsonObject.getString(key);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		return "Information not available";
	}
}
