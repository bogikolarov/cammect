package org.elsys.thesisdiploma.cammect.file;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map.Entry;

import org.elsys.thesisdiploma.cammect.http.HttpRequester;

import android.os.AsyncTask;
import android.util.Log;

public class FileManager {
	public static final String APP_SUBDIRECTORY = "/Cammect/";
	
	private static final String TAG = "FileManager";
	private final String ERROR = "error";
	
	public static boolean fileExists(String path) {
		Log.i(TAG,"fileEexists(\"" + path + "\")");
		final File file = new File(path);
		return file.exists();
	}

	public void uploadFile(File file, String url,
			HashMap<String, String> requestProperties,
			FileUploadCallback callback) {
		try {
			new FileUploader(file, requestProperties, url, callback).execute();
		} catch (MalformedURLException e) {
			e.printStackTrace();
			callback.onError();
		}
	}

	public int downloadFile(String url, String filePath, String fileName, FileDownloadCallback callback) {
		return new FileDownloader(url, filePath, fileName, callback).execute().hashCode();
	}
	
	private class FileUploader extends AsyncTask<Void, Void, String> {
		private final File file;
		private final URL url;
		private final HashMap<String, String> requestProperties;
		private final FileUploadCallback callback;
		
		public FileUploader(File file, HashMap<String, String> requestProperties, String urlString, FileUploadCallback callback) throws MalformedURLException {
			this.file = file;
			this.url = new URL(urlString);
			this.requestProperties = requestProperties;
			this.callback = callback;
		}

		@Override
		protected String doInBackground(Void... params) {
			HttpURLConnection httpURLConnection = null;
			DataOutputStream dataOutputStream = null;

			String responseFromServer = "";

			try {
				httpURLConnection = (HttpURLConnection) this.url.openConnection();
				this.sethttpUrlConnectionProperties(httpURLConnection, this.requestProperties);
				
				dataOutputStream = new DataOutputStream(
						httpURLConnection.getOutputStream());

				this.writePropertiesToDataOutputStream(dataOutputStream);
				this.writeFileToDataOutputStream(dataOutputStream, this.file);

				dataOutputStream.writeBytes(HttpRequester.LINE_END);
				dataOutputStream.writeBytes(HttpRequester.TWO_HYPENS
						+ HttpRequester.BOUNDARY + HttpRequester.TWO_HYPENS
						+ HttpRequester.LINE_END);

				Log.e(TAG, "File is written");
			} catch (MalformedURLException ex) {
				Log.e(TAG, "error: " + ex.getMessage(), ex);
				return ERROR;
			} catch (IOException ioe) {
				Log.e(TAG, "error: " + ioe.getMessage(), ioe);
				return ERROR;
			} finally {
				if (dataOutputStream != null) {
					try {
						dataOutputStream.flush();
						dataOutputStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			try {
				responseFromServer = this.convertInputStreamToString(new DataInputStream(
						httpURLConnection.getInputStream()));
				Log.i(TAG, "FILE UPLOAD RESPONSE: " + responseFromServer);
				return responseFromServer;
			} catch (IOException e) {
				e.printStackTrace();
				return ERROR;
			}
		}

		private void writeFileToDataOutputStream(
				DataOutputStream dataOutputStream, File file)
				throws IOException {
			int bytesRead, bytesAvailable, bufferSize;
			byte[] buffer;
			int maxBufferSize = 1 * 1024 * 1024;
			final FileInputStream fileInputStream = new FileInputStream(file);
			
			Log.i(TAG, "FILE UPLOAD NAME: " + file.getName());
			
			bytesAvailable = fileInputStream.available();
			bufferSize = Math.min(bytesAvailable, maxBufferSize);
			buffer = new byte[bufferSize];
			
			bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			while (bytesRead > 0) {
				Log.i(TAG, "writing " + bytesRead + " bytes");
				dataOutputStream.write(buffer, 0, bufferSize);
				bytesAvailable = fileInputStream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			}
			fileInputStream.close();
		}

		private void writePropertiesToDataOutputStream(
				DataOutputStream dataOutputStream) throws IOException {
			dataOutputStream.writeBytes(HttpRequester.TWO_HYPENS
					+ HttpRequester.BOUNDARY + HttpRequester.LINE_END);
			dataOutputStream
					.writeBytes("Content-Disposition: form-data; name=\"uploadedfile\";filename=\""
							+ this.file.getName() + "\"" + HttpRequester.LINE_END);
			dataOutputStream.writeBytes(HttpRequester.LINE_END);

		}

		private void sethttpUrlConnectionProperties(
				HttpURLConnection httpURLConnection, HashMap<String, String> requestProperties) throws ProtocolException {

			httpURLConnection.setDoInput(true);
			httpURLConnection.setDoOutput(true);
			httpURLConnection.setUseCaches(false);

			httpURLConnection.setRequestMethod(HttpRequester.POST);
			
			for (Entry<String, String> entry : requestProperties.entrySet()) {
				Log.i(TAG, entry.getKey() + ": " + entry.getValue());
				httpURLConnection.setRequestProperty(entry.getKey(), entry.getValue());
			}
		}

		private String convertInputStreamToString(InputStream inputStream)
				throws IOException {
			BufferedReader bufferedReader = new BufferedReader(
					new InputStreamReader(inputStream));
			String line = "";
			StringBuilder result = new StringBuilder();
			
			while ((line = bufferedReader.readLine()) != null) {
				result.append(line);
			}

			inputStream.close();
			return result.toString();
		}

		@Override
		protected void onPostExecute(String result) {
			if (result.contains("error")) {
				this.callback.onError();
			} else {
				this.callback.onFileUploaded(result);
			}
		}	
	}

	private class FileDownloader extends AsyncTask<Void, Void, Void> {
		private String url;
		private String filePath;
		private String fileName;
		private FileDownloadCallback callback;

		private boolean errorOcurred = false;

		public FileDownloader(String url, String filePath, String fileName,
				FileDownloadCallback callback) {
			this.url = url;
			this.filePath = filePath;
			this.fileName = fileName;
			this.callback = callback;
		}

		@Override
		protected void onPreExecute() {
			Log.i(TAG, "DOWNLOAD FILE STARTING");
			Log.i(TAG, "URL: " + this.url);
		}

		@Override
		protected Void doInBackground(Void... params) {
			InputStream inputStream = null;
			OutputStream outputStream = null;

			try {
				final URL url = new URL(this.url);
				final URLConnection urlConnection = url.openConnection();
				urlConnection.connect();

				inputStream = new BufferedInputStream(url.openStream());

				final File file = new File(this.filePath);
				if (!file.exists()) {
					file.mkdirs();
				}

				outputStream = new FileOutputStream(this.filePath
						+ this.fileName);

				byte[] data = new byte[1024];

				if (this.isCancelled()) {
					return null;
				}

				int count;
				while ((count = inputStream.read(data)) != -1) {
					outputStream.write(data, 0, count);
				}

				outputStream.flush();
			} catch (IOException e) {
				this.cancel(true);
				e.printStackTrace();
			} finally {
				try {
					this.closeStreams(inputStream, outputStream);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			return null;
		}

		private void closeStreams(InputStream inputStream,
				OutputStream outputStream) throws IOException {
			if (inputStream != null) {
				inputStream.close();
			}

			if (outputStream != null) {
				outputStream.close();
			}
		}

		@Override
		protected void onCancelled() {
			this.errorOcurred = true;
			this.callback.onFileDownloadError();
		}

		@Override
		protected void onPostExecute(Void result) {
			if (!errorOcurred) {
				this.callback.onFileDownloaded(this.filePath, this.fileName, this.hashCode());
			}
		}
	}
	
}
