package org.elsys.thesisdiploma.cammect.file;

public interface FileUploadCallback {
	public void onFileUploaded(String response);
	public void onError();
}
