package org.elsys.thesisdiploma.cammect.file;

public interface FileDownloadCallback {
	public void onFileDownloaded(String fileDir, String fileName, int threadHashCode);
	public void onFileDownloadError();
}
