package org.elsys.thesisdiploma.cammect.frame;

import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.core.Mat;

public class Frame implements CvCameraViewListener2 {
	private CvCameraViewFrame currentFrame;
	
	@Override
	public void onCameraViewStarted(int width, int height) {
		// DO NOTHING
	}

	@Override
	public void onCameraViewStopped() {
		// DO NOTHING
	}

	@Override
	public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
		this.currentFrame = inputFrame;
		
		return inputFrame.rgba();
	}

	public Mat getCurrentRGBAFrame() {
		return this.currentFrame.rgba();
	}
	
	public Mat getCurrentGrayFrame() {
		return this.currentFrame.gray();
	}
}

