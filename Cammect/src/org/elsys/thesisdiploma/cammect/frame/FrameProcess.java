package org.elsys.thesisdiploma.cammect.frame;

import java.util.HashMap;

import org.elsys.thesisdiploma.cammect.detection.DetectionResultHandler;
import org.elsys.thesisdiploma.cammect.detection.NativeSVMDetector;
import org.elsys.thesisdiploma.cammect.detection.OnDetectionResult;
import org.elsys.thesisdiploma.cammect.detection.ServerDetector;
import org.elsys.thesisdiploma.cammect.http.HttpRequester;
import org.opencv.core.Mat;

import android.util.Log;

public class FrameProcess implements OnDetectionResult {
	public static final int DETECT_BRAND = 0;
	public static final int DETECT_MODEL = 1;
	public static final int DETECT_OBJECT = 2;
	
	public static final String DETECT_NOT_FOUND = "not found";
	
	private final String TAG = getClass().getSimpleName();
	
	private ServerDetector serverDetector;
	private NativeSVMDetector detector;
	private DetectionResultHandler detectionResultHandler;
	
	private int currentDetection = DETECT_BRAND;
	private boolean isDetecting = false;
	
	public FrameProcess(DetectionResultHandler detectionResultHandler) {
		this.serverDetector = new ServerDetector();
		this.detectionResultHandler = detectionResultHandler;
	}
	
	public void brandDetect(String target, Mat frame) {
		Log.i(TAG, "brandDetect()");
		this.isDetecting = true;
		this.serverDetector.detect(frame, target, null, this);
	}
	
	public void modelDetect(String target, Mat frame) {
		Log.i(TAG, "modelDetect()");
		this.isDetecting = true;
		final HashMap<String, String> requestProperties = new HashMap<String, String>();
		requestProperties.put(HttpRequester.HEADER_BRAND, this.detectionResultHandler.getCar().getBrand());
		
		this.serverDetector.detect(frame, target, requestProperties, this);
	}
	
	public void svmDetect(Mat frame) {
		Log.i(TAG, "svmDetect()");
		
		detector = new NativeSVMDetector(frame, this);
		detector.execute();	
	}

	public boolean isDetecting() {		
		if (this.detector != null) {
			return this.detector.isDetecting(); 
		} else {
			return this.isDetecting;
		}
	}
	
	public void setCurrentDetection(int currentDetection) {
		this.currentDetection = currentDetection;
	}
	
	public int getCurrentDetection() {
		return this.currentDetection;
	}

	public void clearDetection() {
		this.currentDetection = FrameProcess.DETECT_BRAND;
		this.detector = null;
	}
	
	private void nextDetection() {
		Log.i(TAG, "nextDetection()");
		if (this.currentDetection < FrameProcess.DETECT_OBJECT) {
			this.currentDetection++;
		}
		Log.i(TAG, "current detection: " + this.currentDetection);
	}
	
	@Override
	public void onDetectionResult(float SVMResponse) {
		this.isDetecting = false;
		this.detectionResultHandler.setCurrentDetection(this.currentDetection);
		this.detectionResultHandler.onDetectionResult(SVMResponse);
	}

	@Override
	public void onDetectionReult(String itemName) {
		this.isDetecting = false;
		
		if (!itemName.contains(FrameProcess.DETECT_NOT_FOUND)) {
			if (this.currentDetection == FrameProcess.DETECT_MODEL) {
				Log.i(TAG, "frame process onDetectionResult: " + itemName);
				this.serverDetector.setCurrentBrand(itemName);
			}

			this.detectionResultHandler.setCurrentDetection(this.currentDetection);
			this.detectionResultHandler.onDetectionReult(itemName);
			this.nextDetection();
		} else {
			this.detectionResultHandler.onDetectionReult(itemName);
		}
	}

	@Override
	public void onError(String message) {
		this.isDetecting = false;
		this.detectionResultHandler.setCurrentDetection(this.currentDetection);
		this.detectionResultHandler.onError(message);
	}
}
