#include <jni.h>
#include <string>
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <opencv2/nonfree/nonfree.hpp>
#include <fstream>
#include <android/log.h>
#include <vector>

#define TAG "NATIVE"

#define LOGI(...)  __android_log_print(ANDROID_LOG_INFO, TAG,__VA_ARGS__)


using namespace cv;
using namespace std;

extern "C" {
	JNIEXPORT jstring JNICALL Java_org_elsys_thesisdiploma_cammect_MainActivity_GetString(
			JNIEnv *env, jobject obj) {
		return env->NewStringUTF("Please touch the screen to detect.");
	}

	JNIEXPORT jfloat JNICALL Java_org_elsys_thesisdiploma_cammect_detection_NativeSVMDetector_SVMDetect(
			JNIEnv *env, jobject obj, jlong addrMatRGB, jstring detectionFilesPath, jstring dictionaryName, jstring classifierName) {
		LOGI("================ NATIVE METHOD CALLED ================");

		cv::initModule_nonfree();

		Mat& matRGBA = *(Mat*) addrMatRGB;
		Mat matBGR;
		cvtColor(matRGBA, matBGR, CV_RGBA2BGR);

		const Ptr<FeatureDetector> detector = FeatureDetector::create("DynamicSURF");
		const Ptr<DescriptorExtractor> descriptors = DescriptorExtractor::create("OpponentSURF");
		const Ptr<DescriptorMatcher> matcher = DescriptorMatcher::create("FlannBased");
		BOWImgDescriptorExtractor bowDE(descriptors, matcher);

		const char* strFromJNI = env->GetStringUTFChars(detectionFilesPath, NULL);
		const string detectionFilesPathPathString(strFromJNI);
		const char* strFromJNI2 = env->GetStringUTFChars(dictionaryName, NULL);
		const string dictionaryNameString(strFromJNI2);
		const char* strFromJNI3 = env->GetStringUTFChars(classifierName, NULL);
		const string classifierNameString(strFromJNI3);

		__android_log_print(ANDROID_LOG_INFO, TAG, (detectionFilesPathPathString + dictionaryNameString).c_str(), 1);

		FileStorage fileStorage(detectionFilesPathPathString + "/" + dictionaryNameString, FileStorage::READ);
		LOGI("read from file storage");
		Mat dictionary;

		fileStorage["steering_wheel_dictionary"] >> dictionary;
		fileStorage.release();
		LOGI("file read");
		bowDE.setVocabulary(dictionary);
		LOGI("set vocabulary");

		Mat features;
		vector<KeyPoint> keypoints;

		detector->detect(matBGR, keypoints);
		__android_log_print(ANDROID_LOG_INFO, TAG, "Detected keypoins: %d", keypoints.size());

		KeyPointsFilter::retainBest(keypoints, 1700);
		LOGI("retainBest");

		bowDE.compute(matBGR, keypoints, features);
		LOGI("compute");

		CvSVM svm;
		svm.load((detectionFilesPathPathString + "/" + classifierNameString).c_str());
		LOGI("detect");

		drawKeypoints(matBGR, keypoints, matBGR);
		LOGI("drawKeypoints");

		LOGI("================ NATIVE METHOD END ================");

		return svm.predict(features);
	}
}
