LOCAL_PATH := $(call my-dir)
OPENCV_PATH := /home/crash-id/Development/SDK/OpenCV-2.4.9-android-sdk/sdk/native/jni

include $(CLEAR_VARS)
LOCAL_MODULE    := nonfree
LOCAL_SRC_FILES := libnonfree.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
OPENCV_CAMERA_MODULES	:= on
OPENCV_INSTALL_MODULES	:= on

include $(OPENCV_PATH)/OpenCV.mk

LOCAL_C_INCLUDES :=				\
	$(LOCAL_PATH)				\
	$(OPENCV_PATH)/include
	
	LOCAL_SRC_FILES :=				\
	SVMDetector.cpp
	
LOCAL_MODULE := svm_detector
LOCAL_CFLAGS := -Werror -O3 -ffast-math
LOCAL_LDLIBS := -llog -ldl
LOCAL_SHARED_LIBRARIES += nonfree

include $(BUILD_SHARED_LIBRARY)